import java.util.*;
public class LongStack {
	
   private LinkedList<Long> mag;

   public static void main (String[] argum) {
	   String s = "35 10 -3 + x 2";
	   LongStack.interpret (s);
   }

   LongStack() {
      mag = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
	   LongStack newStack = new LongStack();
     newStack.mag = (LinkedList<Long>) mag.clone();
     return newStack;
   }

   public boolean stEmpty() {
	   return (mag.size() == 0);
   }

   public void push (long a) {
      mag.addFirst(a);
   }

   public long pop() {
	   if (stEmpty()) throw new RuntimeException("Magasini alat�itumine");
	   return mag.removeFirst();
   } // pop

   public void op (String s) {
	   if (stEmpty()) throw new RuntimeException("Operaator" + s + " jaoks ei j�tku arve");
	   long op2 = pop();
	   if (stEmpty()) throw new RuntimeException("O");
	   long op1 = pop();
	   if (s.equals ("+")) push (op1 + op2);
	   if (s.equals ("-")) push (op1 - op2);
	   if (s.equals ("*")) push (op1 * op2);
	   if (s.equals ("/")) push (op1 / op2);
   }
  
   public long tos() {
	   if (stEmpty()) throw new RuntimeException("Magasinis pole elemente");
	   return mag.getFirst();
   }

   @Override
   public boolean equals (Object o) {
	  if (((LongStack)o).mag.size() != mag.size()) return false;
	  for (int i = 0; i < mag.size(); i++) {
		  if (((LongStack)o).mag.get(i) != mag.get(i)) return false;
	  }
    return true;
   }

   @Override
   public String toString() {
	  if (stEmpty()) return "Tyhi";
	  StringBuffer b = new StringBuffer();
    for (int i=(mag.size()-1); i>=0; i--)
      b.append (String.valueOf (mag.get(i)) + " ");
      return b.toString();
   }

   public static long interpret (String pol) {
	   String operators = "+-*/";
     LongStack stack = new LongStack();
		 pol = pol.replace("\t", "");
		 pol = pol.trim();
		 String[] elements = pol.split("\\s+");
		for (int i = 0; i < elements.length; i++) {
			if (!operators.contains(elements[i])) {
				if(elements[i].trim().matches("-?\\d+(\\.\\d+)?")) {
					stack.push(Long.parseLong(elements[i].trim()));
				} else {
					throw new RuntimeException("Tundmatu operaator "+pol);
				}
			} else {
				if (stack.mag.size() < 2) throw new RuntimeException("Ei j�tku arve avaldise " + pol + " arvutamiseks.");
				long a = stack.pop();
				long b = stack.pop();
				switch (elements[i]) {
				case "+":
					stack.push(a + b);
					break;
				case "-":
					stack.push(b - a);
					break;
				case "*":
					stack.push(a * b);
					break;
				case "/":
					stack.push(b / a);
					break;
				}
			}
		}
		
		if (stack.mag.size() > 1) throw new RuntimeException("Magasini j�i lubatust rohkem arv elemente. " + pol);
		return stack.pop();
   }

}

